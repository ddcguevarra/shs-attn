@extends('layouts.layouts')
@section('title', 'Attendance')

@section('content')

	<div class="container">
		<h3>Attendance | {{ $date }}</h3>

        <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal1">
            Check Date
        </button>

        <br>

        @if(Session::has('message'))
		<div class="container alert alert-danger alert-dismissable">
			<h4 class="alert-heading">Warning!</h4>
			{{Session::get('message')}}
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	  		<span aria-hidden="true">&times;</span>
			</button>
		</div>
		@endif

        <h4>Present</h4>
		<table class="table table-hover">
			<thead>
				<tr>
					<td>Lastname</td>
					<td>Firstname</td>
					<td>Time</td>
				</tr>
			</thead>
			<tbody>
				@foreach($clockedin as $time)
					<tr>
						<td>{{ $time->lastname }}</td>
						<td>{{ $time->firstname }}</td>
						<td>{{ $time->created_at }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		<br>

		<h4>Absent</h4>
		<table class="table table-hover">
			<thead>
				<tr>
					<td>Lastname</td>
					<td>Firstname</td>
					<td>Contact</td>
				</tr>
			</thead>
			<tbody>
				@foreach($students as $time)
					<tr>
						<td>{{ $time->lastname }}</td>
						<td>{{ $time->firstname }}</td>
						<td>{{ $time->contact }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@endsection

@section('modals')
@parent
<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Check Date</h4>
	      </div>
	      <div class="modal-body">
	        {!! Form::open(['route' => 'attendance.check', 'Method' => 'POST']) !!}

			{!! Form::label('Date') !!}
			{!! Form::date('date', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	        {!! form::submit('Check', ['class' => 'btn btn-success'])!!}

	      </div>
	      {!! Form::close() !!}
	    </div>
	  </div>
	</div>
@endsection