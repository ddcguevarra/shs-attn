@extends('layouts.layouts')

@section('title', 'My Web Page')

@section('content')
	<div class = "container">
	<h3>Late Students</h3> 
	<h3>Welcome: {{$usr->firstname}} {{$lname->lastname}}</h3>

	{!! Form::open(['route' => 'mainmenu.store', 'Method' => 'POST']) !!}

	{!! Form::label('Student ID')!!}
	{!! Form::text('student_id', null,['class' => 'form-control','required' => '', 'maxlength' => '20']) !!}

	{!! Form::submit('Submit', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}



	<table class="table table-hover">
		<thead>
			<tr>
				<th>Employee_ID</th>
				<th>Student_ID</th>
				<th>Lastname</th>
				<th>firstname</th>
				<th>date</th>
				<th>ToA</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
		
		</tbody>

	</div>
	
@endsection