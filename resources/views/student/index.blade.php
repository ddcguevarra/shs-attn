@extends('layouts.layouts')

@section('title', 'Student List')

@section('content')


	@if(Session::has('message'))
	<div class="container alert alert-success alert-dismissable">
		<h4 class="alert-heading">Success!</h4>
		{{Session::get('message')}}
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
  		<span aria-hidden="true">&times;</span>
		</button>
	</div>
	@endif

	<div class="container">
		<h3>List of Students</h3>
		<button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal1">
            Add Student
        </button>

		<table class="table table-hover">
			<thead>
				<tr>
					<td>Lastname</td>
					<td>Firstname</td>
					<td>Address</td>
					<td>Birthdate</td>
					<td>Contact</td>
					<td>Section</td>
				</tr>
			</thead>
			<tbody>
				@foreach($students as $student)
					<tr>
						<td>{{ $student->lastname }}</td>
						<td>{{ $student->firstname }}</td>
						<td>{{ $student->address }}</td>
						<td>{{ $student->birthdate }}</td>
						<td>{{ $student->contact }}</td>
						<td>{{ $student->section }}</td>
						<td>
							<div class="btn-group pull-right">
                          		<button class="edit-modal btn btn-success" data-toggle="modal" data-target="#editstudent" data-id="{{$student->id}}" data-barcode="{{$student->barcode}}" data-lastname="{{$student->lastname}}" data-firstname="{{$student->firstname}}" data-address="{{$student->address}}" data-birthdate="{{$student->birthdate}}" data-contact="{{$student->contact}}" data-section="{{$student->section}}">
                            	<span class="glyphicon glyphicon-pencil"></span>
                          		</button>
                  			</div>
					</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		<center>{{ $students->links() }}</center>
	</div>
@endsection


@section('modals')
@parent
<div id="editstudent" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="panel panel-primary">
          <div class="panel-heading">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="panel-title" id="myModalLabel"><b>Edit student</b></h4>
          </div>
        <div class="modal-body">
        
        	 @if(isset($student->id))
            {!! Form::model($student, ['route' => ['student.update', $student->id], 'method' => 'PUT']) !!}

            {!! Form::label('Barcode') !!}
            {!! Form::text('id', null, ['id' => 'uid', 'class' => 'form-control', 'readonly']) !!}

            {!! Form::label('Lastname') !!}
			{!! Form::text('lastname', null, ['id' => 'lname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20', 'autocomplete'=>'off']) !!}

			{!! Form::label('Firstname') !!}
			{!! Form::text('firstname', null, ['id' => 'fname', 'class' => 'form-control', 'required' => '', 'maxlength' => '20', 'autocomplete'=>'off']) !!}

			{!! Form::label('Address') !!}
			{!! Form::text('address', null, ['id' => 'uaddress', 'class' => 'form-control', 'required' => '', 'maxlength' => '100', 'autocomplete'=>'off']) !!}

			{!! Form::label('Birthdate') !!}
			{!! Form::date('birthdate', null, ['id' => 'ubirthdate', 'class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Contact') !!}
			{!! Form::text('contact', null, ['id' => 'ucontact', 'class' => 'form-control', 'required' => '', 'maxlength' => '13', 'autocomplete'=>'off']) !!}

			{!! Form::label('Section') !!}
			{!! Form::text('section', null, ['id' => 'usection', 'class' => 'form-control', 'required' => '', 'maxlength' => '100', 'autocomplete'=>'off']) !!}



                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal" style="margin-top:20px">Cancel</button>
                  {!! Form::submit('Update', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px']) !!}
                </div>
            	{!! Form::close() !!}
            	@endif
        </div>
      </div>
    </div>
</div>    

{{-- Add User Modal --}}
	<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Add student</h4>
	      </div>
	      <div class="modal-body">
	        {!! Form::open(['route' => 'student.store', 'Method' => 'POST']) !!}

			{!! Form::label('Lastname') !!}
			{!! Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20', 'autocomplete'=>'off']) !!}

			{!! Form::label('Firstname') !!}
			{!! Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20', 'autocomplete'=>'off']) !!}

			{!! Form::label('Address') !!}
			{!! Form::text('address', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100', 'autocomplete'=>'off']) !!}

			{!! Form::label('Birthdate') !!}
			{!! Form::date('birthdate', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Contact') !!}
			{!! Form::text('contact', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '13', 'autocomplete'=>'off']) !!}

			{!! Form::label('Section') !!}
			{!! Form::text('section', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100', 'autocomplete'=>'off']) !!}

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	        {!! form::submit('Save', ['class' => 'btn btn-success'])!!}

	      </div>
	      {!! Form::close() !!}
	    </div>
	  </div>
	</div>
@endsection

@section('scripts')
@parent
    {{-- Edit User Modal --}}
    <script type="text/javascript">
        $(document).on('click', '.edit-modal', function() {
            $('#uid').val($(this).data('id'));
            $('#lname').val($(this).data('lastname'));
            $('#fname').val($(this).data('firstname'));
            $('#uaddress').val($(this).data('address'));
            $('#ubirthdate').val($(this).data('birthdate'));
            $('#ucontact').val($(this).data('contact'));
            $('#usection').val($(this).data('section'));

            $('#editstudent').modal('show');
        });
    </script>
@endsection

