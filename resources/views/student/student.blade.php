<!DOCTYPE html>
<html>
<head>
	<title>DTR</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</head>
<body>
	<div class="Container">
		<h3>Student Details</h3>
		
		{!! Form::open(['route' => 'employee.store', 'Method' => 'POST']) !!}

			{!! Form::label('Lastname') !!}
			{!! Form::text('lastname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

			{!! Form::label('Firstname') !!}
			{!! Form::text('firstname', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '20']) !!}

			{!! Form::label('Address') !!}
			{!! Form::text('address', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! Form::label('Birthdate') !!}
			{!! Form::date('birthdate', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '100']) !!}

			{!! form::submit('Save', ['class' => 'btn btn-success', 'style' => 'margin-top: 20px'])!!}


		{!! Form::close() !!}
	</div>
</body>
</html>