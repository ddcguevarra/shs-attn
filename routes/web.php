<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('student','StudentController');
Route::resource('attendance', 'AttendanceController');
Route::resource('employee', 'EmployeeController');

Route::post('attendance/check', 'AttendanceController@check')->name('attendance.check');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function() {
	Route::resource('student','StudentController');
	Route::resource('attendance', 'AttendanceController');
});