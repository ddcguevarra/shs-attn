<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Attendance;
use App\Student;
use Carbon\Carbon;

class AttendanceController extends Controller
{
  	public function index() {

  		$clockedin = Attendance::leftJoin('students', 'attendances.student_id', '=', 'students.id')->select('students.lastname', 'students.firstname', 'attendances.created_at')->whereDate('attendances.created_at',DB::raw('CURDATE()'))->get();
  		$absent = Attendance::whereDate('created_at', DB::raw('CURDATE()'))->select('student_id')->get();
  		$arrabsent = $absent->toArray();
        $students = Student::whereNotIn('id', $arrabsent)->get();
        $add = 1;

        return view('attn.index', compact('clockedin','students'));
    }

    public function store(Request $request) {
        if (!Student::whereId($request->id)->exists()) {
            return redirect()->route('attn.index')->with('message', 'Student Existence in Question');
        }
    	$emp = Student::whereId($request->id)->first();

    	if (!Attendance::where('student_id', $emp->id)->whereDate('created_at',DB::raw('CURDATE()'))->exists()) {
    		$timein = new Attendance;

    		$timein->student_id = $emp->id;

    		$timein->save();
    	}
    	else {
    		return redirect()->route('attendance.index')->with('message', 'Student is already present');
    	}
        return redirect()->route('attendance.index');
    }

    public function show($id)
    {
        
    }

    public function check(Request $request) {
    	$clockedin = Attendance::leftJoin('students', 'attendances.student_id', '=', 'students.id')->select('students.lastname', 'students.firstname', 'attendances.created_at')->whereDate('attendances.created_at', $request->date)->get();
  		$absent = Attendance::whereDate('created_at', $request->date)->select('student_id')->get();
  		$arrabsent = $absent->toArray();
        $students = Student::whereNotIn('id', $arrabsent)->whereDate('created_at', '<=' , $request->date)->get();
        $date = $request->date;

        return view('attn.check', compact('clockedin','students', 'date'));
    }
}
